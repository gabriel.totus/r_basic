import numpy as np
from timeit import default_timer as timer
#!pip install numbapro
from numba import vectorize

@vectorize(["float32(float32,float32)"], target='cuda')
def vectorAdd(a, b):
    #for i in range(a.size):
    #    c[i] = a[i] + b[i]
    return a + b


def main():
	N = 32000000
	A = np.ones(N, dtype=np.float32)
	B = np.ones(N, dtype=np.float32)
	C = np.ones(N, dtype=np.float32)

	start = timer()
	#vectorAdd(A,B,C)
	vectorAdd(A,B)	
	vector_times = timer() - start

	print("c[:5] = " + str(C[:5]))
	print("c[-5:] = " + str(C[-5:]))

	print("time %f " % vector_times)


if __name__ == '__main__':
	main()